# Crypto Hole

Here's a lot of crypto challenges all packed into one. To start, unzip the starting zip file and enter ```NeverLANCTF``` as the password. 

Each correct decryption, besides two, will be prefixed with ```password:```


# Files
A_ffine_Cipher_here_3.zip 


# Flag

flag{crypt0sRphun}
