# Unsecured Login2

#### Description
We caught someone logging into their website, but they didn't check their links when submitting data!

#### Flag
flag{ensure_https_is_always_used}

#### Solution
Open the file in wireshark and find the http post request that hold the flag in the password field.
