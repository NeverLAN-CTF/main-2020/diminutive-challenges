# Unsecured Login

#### Description
We caught someone logging into their website, but they didn't use https!

#### Flag
flag{n0httpsn0l0gin}

#### Solution
Open the file in wireshark and find the http post request that hold the flag in the password field.
