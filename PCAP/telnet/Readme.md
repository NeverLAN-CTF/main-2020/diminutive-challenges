# Telnet

#### Description
It looks like someone hasn't upgraded to ssh yet...

#### Flag
flag{telnet_1s_n0t_secur3}

#### Solution
Open the file in wireshark and find the packet with the flag
