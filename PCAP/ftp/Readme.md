# Unsecured FTP

#### Description
It looks like someone forgot to use a secure version of ftp...

#### Flag
flag{sftp_OR_ftps_not_ftp}

#### Solution
Open the file in wireshark and find the packet with the flat.txt data.
